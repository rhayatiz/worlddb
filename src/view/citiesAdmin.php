<?php
//affichage d'une ville dans le tableau des villes
function cityRow($city){
echo '<tr data-city-id="'.$city->City_Id.'">
      <th scope="row"><a class="font-weight-bold">'.$city->Name.'</a></th>
      <td><a>'.$city->District.'</a></td>
      <td><a>'.$city->Population.'</a></td>
      <td class="button-td">
        <a class="btn delete-button" onclick="deleteVille()">
          <img src="../../img/delete.png" height="25" class="d-inline-block align-top" alt="">
        </a>
        &nbsp&nbsp
        <button id="myBtn" class="btn modify-button" data-toggle="modal" data-target="#modal'.$city->City_Id.'">
          <img src="../../img/modify.png" height="25" class="d-inline-block align-top" alt="">
        </button>
      </td>
    </tr>';
}

//formulaire de modification d'une ville
function modifyCity($city){
  echo '<!-- Modal -->
    <div class="modal fade" id="modal'.$city->City_Id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Modification de la ville <strong>'.$city->Name.'</strong></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form class="form-signin" method="POST" action="http://'.$_SERVER['HTTP_HOST'].'/city/update/'.$city->City_Id.'">
              <div class="form-group row">
                <label for="inputEmail3" class="col-sm-2 col-form-label">Name</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="inputEmail3" name="Name" value="'.$city->Name.'" placeholder="'.$city->Name.'">
                </div>
              </div>

              <div class="form-group row">
                <label for="inputEmail3" class="col-sm-2 col-form-label">District</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="inputEmail3" name="District" value="'.$city->District.'" placeholder="'.$city->District.'">
                </div>
              </div>

              <div class="form-group row">
                <label for="inputEmail3" class="col-sm-2 col-form-label">Population</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="inputEmail3" name="Population" value="'.$city->Population.'" placeholder="'.$city->Population.'">
                </div>
              </div>
            <div class my-4></div>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
            <button type="submit" class="btn btn-primary">Enregistrer</button>
            </form>
          </div>
        </div>
      </div>
    </div>';
}

function addCity($data){
  return '<!-- Modal -->
    <div class="modal fade" id="modalAddCity" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Ajout d\'une ville</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form class="form-signin" method="POST" action="http://'.$_SERVER['HTTP_HOST'].'/city/save">
              <div class="form-group row">
                <label for="inputEmail3" class="col-sm-2 col-form-label">Name</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="inputEmail3" name="Name" placeholder="Nom">
                </div>
              </div>

              <div class="form-group row">
                <label for="inputEmail3" class="col-sm-2 col-form-label">District</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="inputEmail3" name="District" placeholder="District">
                </div>
              </div>

              <div class="form-group row">
                <label for="inputEmail3" class="col-sm-2 col-form-label">Population</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="inputEmail3" name="Population" placeholder="population">
                </div>
              </div>
                  <input type="hidden" name="CountryCode" value="'.$data[0]->CountryCode.'">
            <div class my-4></div>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
            <button type="submit" class="btn btn-primary">Ajouter</button>
            </form>
          </div>
        </div>
      </div>
    </div>';
}


/* Début de la page */

include_once('../src/utils/header.php');
if ($data != null){
  //pour chaque ville, placer un formulaire invisible de modification qui apparait en cliquant sur le bouton .modify-button
  foreach ($data as $city){
    modifyCity($city);    
  }
  //Formulaire d'ajout d'une ville
  echo addCity($data);
  //Debut tableau
  echo '<table class="table table-striped">
  <thead>
      <tr>
        <th scope="col">Name</th>
        <th scope="col">District</th>
        <th scope="col">Population</th>
        <th scope="col" style="padding: 7px"><button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modalAddCity">Ajouter une ville</button></th>
      </tr>
  </thead>
  <tbody>';
  //Liste des villes
  foreach ($data as $city) {
      cityRow($city);
  }
  //Fin tableau
   echo ' </tbody>
  </table>';
}else{
  echo "<h1>Aucune ville n'a étée trouvée</h1>";
}


include_once('../src/utils/footer.php');
