<?php
function countryRow($country){
echo '<tr>
      <th scope="row"><img src="'.$country->Image1.'" height="15"  width="25"><a href="http://'  .  $_SERVER['HTTP_HOST']  .  '/country/show/'  . $country->Country_Id  .  '" >&nbsp;'.$country->Name.'</a></th>
      <td>'.$country->Region.'</td>
      <td>'.$country->IndepYear.'</td>
      <td>'.$country->Population.'</td>
      <td>'.$country->GNP.'</td>
      <td>'.$country->Capital.'</td>
    </tr>';
}


include_once('../src/utils/header.php');

//Debut tableau
echo '<table class="table table-striped">
  <thead>
    <tr>
      <th scope="col">Name</th>
      <th scope="col">Region</th>
      <th scope="col">IndepYear</th>
      <th scope="col">Population</th>
      <th scope="col">GNP</th>
      <th scope="col">Capital</th>
    </tr>
  </thead>
  <tbody>';

foreach ($data as $country) {
    countryRow($country);
}

//Fin tableau
 echo ' </tbody>
</table>';
include_once('../src/utils/footer.php');
