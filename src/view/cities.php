<?php
//affichage d'une ville dans le tableau des villes
function cityRow($city){
echo '<tr data-city-id="'.$city->City_Id.'">
      <th scope="row"><a class="font-weight-bold">'.$city->Name.'</a></th>
      <td><a>'.$city->District.'</a></td>
      <td><a>'.$city->Population.'</a></td>
    </tr>';
}

/* Début de la page */

include_once('../src/utils/header.php');
if ($data != null){
  //Debut tableau
  echo '<table class="table table-striped">
  <thead>
      <tr>
        <th scope="col">Name</th>
        <th scope="col">District</th>
        <th scope="col">Population</th>
      </tr>
  </thead>
  <tbody>';
  //Liste des villes
  foreach ($data as $city) {
      cityRow($city);
  }
  //Fin tableau
   echo ' </tbody>
  </table>';
}else{
  echo "<h1>Aucune ville à été retrouvée</h1>";
}


include_once('../src/utils/footer.php');
