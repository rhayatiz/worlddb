<?php 
if (isset($_SESSION['login'])){
  header('Location: http://'.$_SERVER['HTTP_HOST']);
}
if (isset($data)){
  $message = $data[0];
}

include_once('../src/utils/header.php');

echo '<div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
  <div class="card card-signin my-5">
    <div class="card-body">
      <h5 class="card-title text-center">Connexion</h5>
      <form class="form-signin" method="POST" action="http://'.$_SERVER['HTTP_HOST'].'/connexion">
        <div class="form-label-group">
          <input type="text" id="login" name="login" class="form-control" placeholder="Compte" required autofocus>
          <label for="login">Compte</label>
        </div>

        <div class="form-label-group">
          <input type="password" id="password" name="password" class="form-control" placeholder="Mot de passe" required>
          <label for="password">Mot de passe</label>
        </div>

        <input type="hidden" value="'.$_SERVER['HTTP_REFERER'].'" name="link">

        <hr class="my-4">
        ';
        if (isset($message)){
          echo "<strong style='color:red'>".$message."</strong>";
        }
        echo '
        <button class="btn btn-lg btn-primary btn-block text-uppercase" type="submit">Se connecter</button>
      </form>
    </div>
  </div>
</div>';

include_once('../src/utils/footer.php');