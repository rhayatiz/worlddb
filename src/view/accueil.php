<?php

//Tableau (continent, image1);
$continents =  array
  (
  array("Asia","https://upload.wikimedia.org/wikipedia/commons/thumb/8/80/Asia_%28orthographic_projection%29.svg/1024px-Asia_%28orthographic_projection%29.svg.png"),
  array("Europe","https://upload.wikimedia.org/wikipedia/commons/4/44/Europe_orthographic_Caucasus_Urals_boundary_%28with_borders%29.svg"),
  array("North America","https://upload.wikimedia.org/wikipedia/commons/thumb/4/43/Location_North_America.svg/550px-Location_North_America.svg.png"),
  array("Africa","https://upload.wikimedia.org/wikipedia/commons/8/86/Africa_%28orthographic_projection%29.svg"),
  array("Oceania","https://upload.wikimedia.org/wikipedia/commons/thumb/8/8e/Oceania_%28orthographic_projection%29.svg/541px-Oceania_%28orthographic_projection%29.svg.png"),
  array("Antarctica","https://upload.wikimedia.org/wikipedia/commons/thumb/f/f2/Antarctica_%28orthographic_projection%29.svg/537px-Antarctica_%28orthographic_projection%29.svg.png"),
  array("South America","https://upload.wikimedia.org/wikipedia/commons/thumb/0/0f/South_America_%28orthographic_projection%29.svg/541px-South_America_%28orthographic_projection%29.svg.png")
  );

function card($continent){
    foreach ($continent as $key => $value){
        echo '
      <div class="col-lg-3 col-md-6 mb-4 ">
        <div class="card h-100">
          <a href="continent/'.$value[0].'">
            <img class="card-img-top" src="'.$value[1].'" width="100px" alt="">
          </a>
          <div class="card-body">
          </div>
          <div class="card-footer">
            <a href="continent/'.$value[0].'" class="d-flex justify-content-center btn btn-info font-weight-bold">'.$value[0].'</a>
          </div>
        </div>
      </div>            
';
}
}

include_once('../src/utils/header.php');
      
echo card($continents);

include_once('../src/utils/footer.php');
