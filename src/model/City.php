<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of City
 *
 * @author student
 */
class City {
    
    public $City_Id;
    public $Name;
    public $CountryCode;
    public $District;
    public $Population;
    
    public function getNom() {
        return strtoupper($this->Name);
    }
    
    function getCity_Id() {
    return $this->City_Id;
}

 function getName() {
    return $this->Name;
}

 function getCountryCode() {
    return $this->CountryCode;
}

 function getDistrict() {
    return $this->District;
}

 function getPopulation() {
    return $this->Population;
}

 function setCity_Id($City_Id) {
    $this->City_Id = $City_Id;
}

 function setName($Name) {
    $this->Name = $Name;
}

 function setCountryCode($CountryCode) {
    $this->CountryCode = $CountryCode;
}

 function setDistrict($District) {
    $this->District = $District;
}

 function setPopulation($Population) {
    $this->Population = $Population;
}

}



