<?php

require_once 'singleton.php';
require_once 'User.php';

Class DAOUser {
   
   private $cnx;
   
   public function __construct() {
       $this->cnx = Singleton::getInstance() -> cnx;
   }

   /**
    * Vérifie si le combo $login/$passwd est équivaut à celui qui est dans la bdd et retourne TRUE si c'est le cas
    */
   public function login($Login, $passwd): bool{

        $requete = $this->cnx -> prepare("SELECT * FROM user WHERE Login = :Login");
        $requete -> bindValue(':Login', $Login, PDO::PARAM_STR);
        $requete -> execute();
        $user = $requete -> fetchObject('User');
        if ($user != null){
            if (password_verify($passwd, $user->psswd)){
                $return = true;                
            }else{
                $return = false;
            }
        }else{
            $return = false;
        }
        
        return $return;
        
   }

}