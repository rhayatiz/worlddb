<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//Fichier de test des DAOs

//////////////////////////////////////////////////////////// TEST DE DAOCITY ////////////////////////////////////////////////////////////
require_once 'DAOCity.php';
$daocity = new DAOCity();

//find OK
// var_dump($daocity->find(2));
// echo "<pre>";
// print_r($daocity->findByNameStartingwith('ly'));
// echo "</pre>";
//
//
////remove OK
//$daocity->remove(9);
//
//
////save OK
//$lion = new City();
//$lion->Name = 'Lion';
//$lion->District = 'Rhone Alpes';
//$lion->CountryCode = 'FRA';
//$lion->Population = '6969';
//$daocity->save($lion);
//echo "<br/>>>";
//var_dump($lion);
//
//
////update OK : on récupère une city, puis on modifie son nom et on fait un update, ensuite on vérifie si le nom a bien été changé
//$qanhadar = $daocity->find(2);
//$qanhadar->setName('Qandish');
//$daocity->update($qanhadar);
//echo "<br/>>>";
//var_dump($qanhadar);
//
////findAll OK
echo "<pre>";
var_dump($daocity->findAll());
echo "</pre>";
//
//
////count OK :: compter le nombre de villes total
//echo "<br/>count : ".$daocity->count();
//
//
////findFromCountry OK :: les villes qu'appartiennent au pays 'France'
//echo "<br> findFromCountry : France";
//echo "<pre>";
//var_dump($daocity->findFromCountry('France'));
//echo "</pre>";
//
//
////findByName OK :: les villes nommées 'Lion'
//echo "<br> findByName : Lion";
//echo "<pre>";
//var_dump($daocity->findByName('Lion'));
//echo "</pre>";
//
////findByNameStartingWith OK :: les villes dont le nom commence par 'Ly'
//echo "<br> findByNameStartingWith : Ly";
//echo "<pre>";
//var_dump($daocity->findByNameStartingWith('Ly'));
//echo "</pre>";

//////////////////////////////////////////////////////////// TEST DE DAOCountry ////////////////////////////////////////////////////////////
// require_once 'DAOCountry.php';
// $daocountry = new DAOCountry();
//
//
////getCountryById OK
//echo "<br/> getCountryById";
//echo "<pre>";
//var_dump($daocountry->getCountryById(2));
//echo "</pre>";
//
////remove  OK
//$daocountry->remove(22);
//
////save NOT OK <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
//$chinatown = new Country();
//$chinatown->Code = 'CNT';
//$chinatown->Name = 'Chinatown';
//$chinatown->Region = 'Funkytown';
//$chinatown->LocalName = 'Chin chin bang bang';
//$chinatown->GovernmentForm = 'Mildly dictatorship';
//$chinatown->Code2 = 'CT';
//
//$daocountry->save($chinatown);
//
////update  OK
//$ethiopia = $daocountry->getCountryById(69);
//$ethiopia->setName('Utopia');
//$daocountry->update($ethiopia);
//
////findAll OK
//echo "<br/>";
//echo "<pre>";
//var_dump($daocountry->findAll());
//echo "</pre>";
//
////Count OK
//echo "<br/> Count country : ".$daocountry->count();
//
////findByName OK
//echo "<br/>";
//echo "<pre>";
//var_dump($daocountry->findByName('Morocco'));
//echo "</pre>";
//
//findByNameStartingWith OK
//echo "<br/>";
//echo "<pre>";
//var_dump($daocountry->findByNameStartingwith('Cana'));
//echo "</pre>";

////findFromContinent OK
//echo "findFromContinent<br/>";
//echo "<pre>";
//var_dump($daocountry->findFromContinent('Asia'));
//echo "</pre>";


//////////////////////////////////////////////////////////// TEST DE DAOLanguage ////////////////////////////////////////////////////////////
//require_once'DAOLanguage.php';
//$daolanguage = new DAOLanguage();
//
//
////find OK
//echo "<pre>";
//var_dump($daolanguage->find(60));
//echo "</pre>";
//
////remove OK :: find69 ne retourne plus rien => le language #69 a bien été supprimé
//$daolanguage->remove(69);
//
////save OK
//$rancais = new CountryLanguage();
//$rancais->setCountryCode("FRA");
//$rancais->setIsOfficial("F");
//$rancais->setLanguage("Le Rançais");
//$rancais->setPercentage(0.01);
//////on sauvegarde le langage, et on recupere l'objet retourné que save() est censé retourner apres avoir affecté un id a ce dernier
//$newrancais = $daolanguage->save($rancais);
//echo $newrancais->getCountryLanguage_Id();
//
//
////update NOT OK <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
//$rancais997 = $daolanguage->find(997);
//echo "<pre>";
//var_dump($rancais997);
//echo "</pre>";
//$rancais997->setLanguage("rancais n2 trop kewl");
//echo "<pre>";
//var_dump($rancais997);
//echo "</pre>";
////echo $daolanguage->update($rancais997);
//
////findAll OK
//echo "<br/>findall";
//echo "<pre>";
//var_dump($daolanguage->findAll());
//echo "</pre>";
//
//
////count OK
//echo "count countrylanguage : ".$daolanguage->Count();
//
//
////findByNameLanguage OK
//echo "<pre>";
//var_dump($daolanguage->FindByNameLanguage("English"));
//echo "</pre>";
//
//
////FindByNameLanguageStartingWith
//echo "<pre>findbynamestartingwith He";
//var_dump($daolanguage->FindByNameLanguageStartingWith("He"));
//echo "</pre>";