<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Language
 *
 * @author student
 */
class CountryLanguage {
    public $CountryLanguage_Id;
    public $CountryCode;
    public $Language;
    public $IsOfficial;
    public $Percentage;
    
    function getCountryLanguage_Id() {
        return $this->CountryLanguage_Id;
    }

    function getCountryCode() {
        return $this->CountryCode;
    }

    function getLanguage() {
        return $this->Language;
    }

    function getIsOfficial() {
        return $this->IsOfficial;
    }

    function getPercentage() {
        return $this->Percentage;
    }

    function setCountryLanguage_Id($CountryLanguage_Id) {
        $this->CountryLanguage_Id = $CountryLanguage_Id;
    }

    function setCountryCode($CountryCode) {
        $this->CountryCode = $CountryCode;
    }

    function setLanguage($Language) {
        $this->Language = $Language;
    }

    function setIsOfficial($IsOfficial) {
        $this->IsOfficial = $IsOfficial;
    }

    function setPercentage($Percentage) {
        $this->Percentage = $Percentage;
    }


}
