<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Country
 *
 * @author student
 */
class Country {
    
    public $Country_Id;
    public $Code;
    public $Name;
    public $Continent;
    public $Region;
    public $SurfaceArea;
    public $IndepYear;
    public $Population;
    public $LifeExpectancy;
    public $GNP;
    public $GNPOld;
    public $LocalName;
    public $GovernmentForm;
    public $HeadOfState;
    public $Capital;
    public $Code2;
    public $Image1;
    public $Image2;
    


    public function getNom() {
        return strtoupper($this->Name);
    }
    function getCountry_Id() {
        return $this->Country_Id;
    }

    function getCode() {
        return $this->Code;
    }

    function getName() {
        return $this->Name;
    }

    function getContinent() {
        return $this->Continent;
    }

    function getRegion() {
        return $this->Region;
    }

    function getSurfaceArea() {
        return $this->SurfaceArea;
    }

    function getIndepYear() {
        return $this->IndepYear;
    }

    function getPopulation() {
        return $this->Population;
    }

    function getLifeExpectancy() {
        return $this->LifeExpectancy;
    }

    function getGNP() {
        return $this->GNP;
    }

    function getGNPOld() {
        return $this->GNPOld;
    }

    function getLocalName() {
        return $this->LocalName;
    }

    function getGovernmentForm() {
        return $this->GovernmentForm;
    }

    function getHeadOfState() {
        return $this->HeadOfState;
    }

    function getCapital() {
        return $this->Capital;
    }

    function getCode2() {
        return $this->Code2;
    }

    function getImage1() {
        return $this->Image1;
    }

    function getImage2() {
        return $this->Image2;
    }

    function setCountry_Id($Country_Id) {
        $this->Country_Id = $Country_Id;
    }

    function setCode($Code) {
        $this->Code = $Code;
    }

    function setName($Name) {
        $this->Name = $Name;
    }

    function setContinent($Continent) {
        $this->Continent = $Continent;
    }

    function setRegion($Region) {
        $this->Region = $Region;
    }

    function setSurfaceArea($SurfaceArea) {
        $this->SurfaceArea = $SurfaceArea;
    }

    function setIndepYear($IndepYear) {
        $this->IndepYear = $IndepYear;
    }

    function setPopulation($Population) {
        $this->Population = $Population;
    }

    function setLifeExpectancy($LifeExpectancy) {
        $this->LifeExpectancy = $LifeExpectancy;
    }

    function setGNP($GNP) {
        $this->GNP = $GNP;
    }

    function setGNPOld($GNPOld) {
        $this->GNPOld = $GNPOld;
    }

    function setLocalName($LocalName) {
        $this->LocalName = $LocalName;
    }

    function setGovernmentForm($GovernmentForm) {
        $this->GovernmentForm = $GovernmentForm;
    }

    function setHeadOfState($HeadOfState) {
        $this->HeadOfState = $HeadOfState;
    }

    function setCapital($Capital) {
        $this->Capital = $Capital;
    }

    function setCode2($Code2) {
        $this->Code2 = $Code2;
    }

    function setImage1($Image1) {
        $this->Image1 = $Image1;
    }

    function setImage2($Image2) {
        $this->Image2 = $Image2;
    }


}
