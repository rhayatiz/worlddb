<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
 require_once 'singleton.php';
 require_once 'City.php';
 
 //constructeur reçoit cnx singleton //getcitybyID
Class DAOCity {
    
    private $cnx;
    
    public function __construct() {
        $this->cnx = Singleton::getInstance() -> cnx;
    }

    function find($id) : object {

            $requete = $this->cnx -> prepare("SELECT * FROM city WHERE City_Id=:id");
            $requete -> bindValue(':id', $id, PDO::PARAM_INT);
            $requete -> execute();

            $result = $requete->fetchObject("City");
            return $result;
    }
    
    /** Methodes crud **/
    
    public function remove($id){

            $requete = $this->cnx -> prepare("DELETE FROM city WHERE City_Id=:id");
            $requete -> bindValue(':id', $id, PDO::PARAM_INT);
            $requete -> execute();
    }
    
    //Insérer une ville (objet City) dans la bdd (City_Id est un auto incrément)
    public function save(City $city){
       
        $cnx=$this->cnx;
       
        $Name=$city->Name;
        $CountryCode=$city->CountryCode;
        $District=$city->District;
        $Population=$city->Population;
       
        //requete sql
        $SQLS="INSERT INTO city (Name,CountryCode,District,Population) VALUES (:Name,:CountryCode,:District,:Population)";
       
        //prepare statement
        $prepareStatementSave=$cnx->prepare($SQLS);
        $prepareStatementSave->bindValue(":Name",$Name, PDO::PARAM_STR);
        $prepareStatementSave->bindValue(":CountryCode",$CountryCode, PDO::PARAM_STR);
        $prepareStatementSave->bindValue(":District",$District, PDO::PARAM_STR);
        $prepareStatementSave->bindValue(":Population",$Population, PDO::PARAM_INT);
    
        $prepareStatementSave->execute();
    }
    
    //modifier une ville
    public function update(City $city){
        
        $cnx=$this->cnx;
       
        $id=$city->City_Id;
        $Name=$city->Name;
        $CountryCode=$city->CountryCode;
        $District=$city->District;
        $Population=$city->Population;
       
        //requete sql
        $SQLU="UPDATE city SET Name=:Name, CountryCode=:CountryCode, District=:District, Population=:Population WHERE City_Id=:id";
       
        //prepare statement
        $prepareStatementUpdate=$cnx->prepare($SQLU);
        $prepareStatementUpdate->bindValue(":id",$id, PDO::PARAM_INT);
        $prepareStatementUpdate->bindValue(":Name",$Name, PDO::PARAM_STR);
        $prepareStatementUpdate->bindValue(":CountryCode",$CountryCode, PDO::PARAM_STR);
        $prepareStatementUpdate->bindValue(":District",$District, PDO::PARAM_STR);
        $prepareStatementUpdate->bindValue(":Population",$Population, PDO::PARAM_INT);

        $prepareStatementUpdate->execute();
    }
    
    //Afficher toutes les villes
    public function findAll() :Array {

            $requete = $this->cnx -> prepare("SELECT * FROM city");
            $requete -> execute();
            
            $cities = array();
            while ( $result = $requete->fetchObject('City') ){
                $cities[] = $result; 
            };
            return $cities;
        
    }
    
    public function count() {

            $requete = $this->cnx->prepare("SELECT COUNT(*) AS nb FROM city");
            $requete -> execute();

            $result = $requete->fetch(PDO::FETCH_ASSOC);
            return intval($result['nb']);
    }
    
    /**   Méthodes facultatives **/
    
    //RETOURNE UN TABLEAU VIDE?
//    public function findFromCountry(string $country) : Array {
//
//            $requete = $this->cnx -> prepare("SELECT * FROM city WHERE CountryCode=(SELECT Code FROM country WHERE Name =:country)");
//            $requete -> bindValue(':country', $country, PDO::PARAM_STR);
//            $requete -> execute();
//
//            $cities = array();
//            while ( $result = $requete->fetchObject('City') ){
//                $cities[] = $result; 
//            };
//            return $cities;
//    }
    
    public function findFromCountry(int $Country_Id) : Array {

            $requete = $this->cnx -> prepare("SELECT * FROM city WHERE CountryCode=(SELECT Code FROM country WHERE Country_Id =:Country_Id)");
            $requete -> bindValue(':Country_Id', $Country_Id, PDO::PARAM_INT);
            $requete -> execute();

            $cities = array();
            while ( $result = $requete->fetchObject('City') ){
                $cities[] = $result; 
            };
            return $cities;
    }
    
    public function findByName(string $name) :Array {

            $requete = $this->cnx -> prepare("SELECT * FROM city WHERE Name = :name");
            $requete -> bindValue(':name', $name, PDO::PARAM_STR);
            $requete -> execute();

            $cities = array();
            while ( $result = $requete->fetchObject('City') ){
                $cities[] = $result; 
            }; 
            return $cities;
    
    }
    
    public function findByNameStartingwith(string $pattern) :Array{

            $requete = $this->cnx -> prepare("SELECT * FROM city WHERE UPPER(Name) LIKE CONCAT(:pattern, '%')");
            $requete -> bindValue(':pattern', strtoupper($pattern), PDO::PARAM_STR);
            $requete -> execute();

            $cities = array();
            while ( $result = $requete->fetchObject('City') ){
                $cities[] = $result; 
            }; 
            return $cities; 
    }
    
    
}



