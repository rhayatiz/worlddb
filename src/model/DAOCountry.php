<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
 require_once 'singleton.php';
 require_once 'Country.php';
 
 //constructeur reçoit cnx singleton //getcitybyID
Class DAOCountry {
    
    private $cnx;
    
    public function __construct() {
        $this->cnx = Singleton::getInstance() -> cnx;
    }    
    
    /**
     * Retourne l'objet de classe Country qui a $id comme id
     * @param type $id
     * @return \Country
     */
    function getCountryById($id) :Country{

            $requete = $this->cnx -> prepare("SELECT * FROM country WHERE Country_Id=:id");
            $requete -> bindValue(':id', $id, PDO::PARAM_INT);
            $requete -> execute();

            $result = $requete->fetchObject('Country');
            
            return $result;
    }
    
    /** Methodes crud **/
    /**
     * Supprime le pays dont l'id est $id
     * @param type $id
     */
    public function remove($id){
        //Supression des villes du pays (contrainte)
            $requete = $this->cnx -> prepare("DELETE FROM city WHERE CountryCode=(SELECT Code FROM country WHERE Country_Id =:id)");
            $requete -> bindValue(':id', $id, PDO::PARAM_INT);
            $requete -> execute();
            
        //Supression des langages du pays (contrainte)
            $requete = $this->cnx -> prepare("DELETE FROM countrylanguage WHERE CountryCode=(SELECT Code FROM country WHERE Country_Id =:id)");
            $requete -> bindValue(':id', $id, PDO::PARAM_INT);
            $requete -> execute();
         
         //Supression du pays
            $requete = $this->cnx -> prepare("DELETE FROM country WHERE Country_Id=:id");
            $requete -> bindValue(':id', $id, PDO::PARAM_INT);
            $requete -> execute();
    }
    
    //Insérer un pays
    public function save(Country $Country){
        $Code=$Country->Code;
        $Name=$Country->Name;
        $Continent=$Country->Continent;
        $Region=$Country->Region;
        $SurfaceArea=$Country->SurfaceArea;
        $IndepYear=$Country->IndepYear;
        $Population=$Country->Population;
        $LifeExpectancy=$Country->LifeExpectancy;
        $GNP=$Country->GNP;
        $GNPOld=$Country->GNPOld;
        $LocalName=$Country->LocalName;
        $GovernmentForm=$Country->GovernmentForm;
        $HeadOfState=$Country->HeadOfState;
        $Capital=$Country->Capital;
        $Code2=$Country->Code2;
        $Image1=$Country->Image1;
        $Image2=$Country->Image2;
        
        echo "image1 : ".$Image1;
        //requete sql
        $SQLS="INSERT INTO country (Code,Name, Continent,Region,SurfaceArea,IndepYear,Population,LifeExpectancy,GNP,GNPOld,LocalName,GovernmentForm,HeadOfState,Capital,Code2,Image1,Image2) VALUES (:Code,:Name, :Continent,:Region,:SurfaceArea,:IndepYear,:Population,:LifeExpectancy,:GNP,:GNPOld,:LocalName,:GovernmentForm,:HeadOfState,:Capital,:Code2,:Image1,:Image2)";
        
         //prepare statement
        $prepareStatementSave=$this->cnx->prepare($SQLS);
        
        $prepareStatementSave->bindValue(":Code",$Code);
        $prepareStatementSave->bindValue(":Name",$Name);
        $prepareStatementSave->bindValue(":Continent",$Continent);
        $prepareStatementSave->bindValue(":Region",$Region);
        $prepareStatementSave->bindValue(":SurfaceArea",$SurfaceArea);
        $prepareStatementSave->bindValue(":IndepYear",$IndepYear);
        $prepareStatementSave->bindValue(":Population",$Population);
        $prepareStatementSave->bindValue(":LifeExpectancy", $LifeExpectancy);
        $prepareStatementSave->bindValue(":GNP", $GNP);
        $prepareStatementSave->bindValue(":GNPOld", $GNPOld);
        $prepareStatementSave->bindValue(":LocalName", $LocalName);
        $prepareStatementSave->bindValue(":GovernmentForm", $GovernmentForm);
        $prepareStatementSave->bindValue(":HeadOfState", $HeadOfState);
        $prepareStatementSave->bindValue(":Capital", $Capital);
        $prepareStatementSave->bindValue(":Code2", $Code2);
        $prepareStatementSave->bindValue(":Image1", $Image1);
        $prepareStatementSave->bindValue(":Image2", $Image2);
        $prepareStatementSave->execute();

   
    }
    
    //modifier un pays
    public function update(Country $Country){
        $Country_Id=$Country->Country_Id;
        $Code=$Country->Code;
        $Name=$Country->Name;
        $Continent=$Country->Continent;
        $Region=$Country->Region;
        $SurfaceArea=$Country->SurfaceArea;
        $IndepYear=$Country->IndepYear;
        $Population=$Country->Population;
        $LifeExpectancy=$Country->LifeExpectancy;
        $GNP=$Country->GNP;
        $GNPOld=$Country->GNPOld;
        $LocalName=$Country->LocalName;
        $GovernmentForm=$Country->GovernmentForm;
        $HeadOfState=$Country->HeadOfState;
        $Capital=$Country->Capital;
        $Code2=$Country->Code2;
        $Image1=$Country->Image1;
        $Image2=$Country->Image2;
        
        
        //requete sql
        $SQLU="UPDATE country SET Code=:Code, Name=:Name, Continent=:Continent, Region=:Region, SurfaceArea=:SurfaceArea, IndepYear=:IndepYear, Population=:Population, LifeExpectancy=:LifeExpectancy, "
                . "GNP=:GNP, GNPOld=:GNPOld, LocalName=:LocalName, GovernmentForm=:GovernmentForm, HeadOfState=:HeadOfState, Capital=:Capital, Code2=:Code2, Image1=:Image1, Image2=:Image2 WHERE Country_Id=:Country_Id";

        //prepare statement
        $prepareStatementSave=$this->cnx->prepare($SQLU);
        
        $prepareStatementSave->bindValue(":Country_Id",$Country_Id);
        $prepareStatementSave->bindValue(":Code",$Code);
        $prepareStatementSave->bindValue(":Name",$Name);
        $prepareStatementSave->bindValue(":Continent",$Continent);
        $prepareStatementSave->bindValue(":Region",$Region);
        $prepareStatementSave->bindValue(":SurfaceArea",$SurfaceArea);
        $prepareStatementSave->bindValue(":IndepYear",$IndepYear);
        $prepareStatementSave->bindValue(":Population",$Population);
        $prepareStatementSave->bindValue(":LifeExpectancy", $LifeExpectancy);
        $prepareStatementSave->bindValue(":GNP", $GNP);
        $prepareStatementSave->bindValue(":GNPOld", $GNPOld);
        $prepareStatementSave->bindValue(":LocalName", $LocalName);
        $prepareStatementSave->bindValue(":GovernmentForm", $GovernmentForm);
        $prepareStatementSave->bindValue(":HeadOfState", $HeadOfState);
        $prepareStatementSave->bindValue(":Capital", $Capital);
        $prepareStatementSave->bindValue(":Code2", $Code2);
        $prepareStatementSave->bindValue(":Image1", $Image1);
        $prepareStatementSave->bindValue(":Image2", $Image2);
        $prepareStatementSave->execute();
        
    }
    
    //Afficher tout les pays
    public function findAll() :Array {

            $requete = $this->cnx -> prepare("SELECT * FROM country");
            $requete -> execute();
            
            $countries = array();
            while ( $result = $requete->fetchObject('Country') ){
                $countries[] = $result; 
            };
            return $countries;
            
    }
    
    //afficher les pays d'un continent
    public function findFromContinent($Continent) :Array {

            $requete = $this->cnx -> prepare("SELECT * FROM country WHERE Continent = :Continent");
            $requete->bindValue(":Continent", $Continent);
            $requete -> execute();
            
            $countries = array();
            while ( $result = $requete->fetchObject('Country') ){
                $countries[] = $result; 
            };
            foreach ($countries as $country){
                $requete2 = $this->cnx -> prepare("SELECT * FROM city WHERE City_Id = :City_Id");
                $requete2->bindValue(":City_Id", $country->Capital);
                $requete2-> execute();
                
                $lacapitale = $requete2->fetchAll();
                $country->Capital = $lacapitale[0]['Name'];
            }
            return $countries;
            
    }
    
    public function count() {
        
            $requete = $this->cnx->prepare("SELECT COUNT(*) AS nb FROM country;");
            $requete -> execute();

            $result = $requete->fetch(PDO::FETCH_ASSOC);
            return intval($result['nb']);
            
    }
    
    /**   Méthodes facultatives **/    
    public function findByName(string $name) :Array {
   
            $requete = $this->cnx -> prepare("SELECT * FROM country WHERE Name = :name");
            $requete -> bindValue(':name', $name, PDO::PARAM_STR);
            $requete -> execute();

            $countries = array();
            while ( $result = $requete->fetchObject('Country') ){
                $countries[] = $result; 
            }; 
            return $countries;
            
    }
    
    public function findByNameStartingwith(string $pattern) :Array{
                
            $requete = $this->cnx -> prepare("SELECT * FROM country WHERE Name LIKE CONCAT(:pattern, '%')");
            $requete -> bindValue(':pattern', $pattern, PDO::PARAM_STR);
            $requete -> execute();
                    
            $countries = array();
            while ( $result = $requete->fetchObject('Country') ){
                $countries[] = $result; 
            }; 
            return $countries;
    }
    
    
}



