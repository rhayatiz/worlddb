<?php

 require_once 'singleton.php';
 require_once 'CountryLanguage.php';

Class DAOLanguage {
    
    private $cnx;
    
    public function __construct() {
        $this->cnx = Singleton::getInstance() -> cnx;
    }
    
    
    public function find($id) : CountryLanguage {
        
        $requete = $this->cnx->prepare("SELECT * FROM countrylanguage WHERE CountryLanguage_Id=:id");
        $requete->bindValue(":id", $id, PDO::PARAM_INT);
        $requete->execute();
        
        $result = $requete->fetchObject("CountryLanguage");
        
        return $result;
        
    }
    
    
    public function remove($id) {
        
        $requete  = $this->cnx->prepare("DELETE FROM countrylanguage WHERE CountryLanguage_id=:id");
        $requete->bindValue(":id", $id);
        $requete->execute();
        
    }
    
    /**
     * Insère un nouveau countrylanguage dans BdD, et affecte a l'objet $countrylanguage l'id que la BDD vient d'insérer
     * @param object $countrylanguage
     * @return object
     */
     public function save(CountryLanguage $countrylanguage) : object{
         
        $requete = $this->cnx->prepare("INSERT INTO countrylanguage (CountryCode, Language,IsOfficial, Percentage) VALUES(:CountryCode, :Language, :IsOfficial, :Percentage)");
        $requete->bindValue(":CountryCode", $countrylanguage->CountryCode);
        $requete->bindValue(":Language", $countrylanguage->Language);
        $requete->bindValue(":IsOfficial", $countrylanguage->IsOfficial);
        $requete->bindValue(":Percentage", $countrylanguage->Percentage);
        
        if($requete->execute()) {
            $last_id = $this->cnx->lastInsertId();
            $countrylanguage->setCountryLanguage_Id($last_id);
        }
        
            return $countrylanguage;
    }
    
    
    public function update(CountryLanguage $countrylanguage):bool{
        $SQL="UPDATE countrylanguage SET  CountryCode=:CountryCode, Language=:Language,  IsOfficial=:IsOfficial, Percentage=:Percentage WHERE CountryLanguage_Id=:id";
        $requete = $this->cnx->prepare($SQL);
        $requete->bindValue(":CountryCode", $countrylanguage->CountryCode);
        $requete->bindValue(":Language", $countrylanguage->Language);
        $requete->bindValue(":IsOfficial", $countrylanguage->IsOfficial);
        $requete->bindValue(":Percentage", $countrylanguage->Percentage);
        $requete->bindValue(":id", $countrylanguage->CountryLanguage_Id);
        
        return $requete->execute();
    }
    
    /**
     * retourne un tableau d'objets CountryLanguage présents dans la BdD
     * @return array
     */
    public function findAll ():Array{
        $requete = $this->cnx->prepare("SELECT * FROM countrylanguage ");
        $requete->execute();
          
        $languages = array();
        while ($unlanguage = $requete->fetchObject('CountryLanguage')){
            $languages[] = $unlanguage;
        }
        
        return $languages;
    }
    
    /**
     * Retourne en entier le nombre de languages présents dans la BdD
     * @return int
     */
    public function Count():int{
        
        $requete=$this->cnx->prepare("SELECT count(CountryLanguage_Id) AS nb FROM countrylanguage");
        $requete->execute();
        $result=$requete->fetch(PDO::PARAM_STR);
        return intval($result['nb']);
        
    }
    
    /**
     * Retourne une liste des pays qui parlent le langage $language
     * @param string $language
     * @return array
     */
    public function FindByNameLanguage(string $language) : Array {
        
//        $requete = $this->cnx->prepare("SELECT * FROM countrylanguage WHERE Language = :Language");
        $requete=$this->cnx->prepare("SELECT * FROM country WHERE Code IN (SELECT CountryCode FROM countrylanguage where language = :language)");
        $requete->bindValue(":language",$language);
        $requete->execute();
        
        $countries = array();
        while ($country = $requete->fetchObject("CountryLanguage")){
            $countries[] = $country;
        }
        return $countries;
    }
    
    /**
     * Retourne une liste des langages qui commencent par $pattern
     * @param string $pattern
     * @return array
     */
    public function FindByNameLanguageStartingWith(string $pattern) : Array {
        
        $SQL="SELECT * FROM countrylanguage WHERE Language LIKE CONCAT(:pattern, '%')";
        $requete = $this->cnx->prepare($SQL);
        $requete->bindValue(":pattern",$pattern);
        $requete->execute();
        
        $languages = array();
        while ($language = $requete->fetchObject("CountryLanguage")){
            $languages[] = $language;
        }
        return $languages;
        
    }
}