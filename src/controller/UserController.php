<?php

class UserController{
    
    public function verify($data){
        $login = $data[0];
        $password = $data[1];

        $daouser = new DAOUser();

        //si c'est les indentifiants sont corrects
        if($daouser->login($login, $password)){
            $_SESSION['login'] = $login;
            //renvoyer l'utilisateur vers la page où il était avant qu'il se connecte
            header('Location: '.$_POST['link']);
        }else{
            //Afficher un message d'erreur si les identifiants sont incorrects
            Renderer::render('connexion', array('Identifiants erronés'));
        }

    } 

}