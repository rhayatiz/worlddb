<?php

class CityController {
    
    public function display($id = null) {

        $daocity = new DAOCity();
        if ($id == 'all'){
            if (isset($_SESSION['login'])){
                Renderer::render('citiesAdmin', $daocity->findAll());
            }else{
                Renderer::render('cities', $daocity->findAll());
            }
        }else{
            Renderer::render('city', array($daocity->find($id)));
        }
        
    }

    public function search($pattern){

        $daocity = new DAOCity();
        if (isset($_SESSION['login'])){
            Renderer::render('citiesAdmin', $daocity->findByNameStartingwith($pattern));
        }else{
            Renderer::render('cities', $daocity->findByNameStartingwith($pattern));
        }

    }

    public function delete($id){

        $daocity = new DAOCity();
        $daocity->remove($id);

    }

    public function update($updateinfo)
    {
        $daocity = new DAOCity();

        $id = $updateinfo[0];
        $newName = $updateinfo[1];
        $newDistrict = $updateinfo[2];
        $newPopulation = $updateinfo[3];

        $city = $daocity->find($id);
        $city->Name = $newName;
        $city->District = $newDistrict;
        $city->Population = $newPopulation;

        $daocity->update($city);
        header('Location: '.$updateinfo[4]);
    }

    public function save($saveinfo) {

        $daocity = new DAOCity();

        $Name = $saveinfo[0];
        $District = $saveinfo[1];
        $Population = $saveinfo[2];
        $CountryCode = $saveinfo[3];

        $c = new City();
        $c->setName($Name);
        $c->setDistrict($District);
        $c->setPopulation($Population);
        $c->setCountryCode($CountryCode);
        $daocity->save($c);

        header('Location: '.$saveinfo[4]);

    }

}
