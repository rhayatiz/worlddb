<?php

class CountryController {
    
    public function display($continent) {

        $daocountry = new DAOCountry();
        if ($continent == 'all') {
            Renderer::render('countries', $daocountry->findAll());
        }else{
            Renderer::render('countries', $daocountry->findFromContinent($continent));
        }
    }

    public function show($country) {

        $daocity = new DAOCity();
        $cities = $daocity->findFromCountry($country);
        if (isset($_SESSION['login'])){
            Renderer::render('citiesAdmin', $cities);
        }else{
            Renderer::render('cities', $cities);
        }
    }
    
}
