<?php
session_start();

echo '
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>World DB</title>
        <link rel="stylesheet" href="../../css/bootstrap.min.css">
        <link rel="stylesheet" href="../../css/style.css">
        <script src="../../js/jquery-3.4.1.min.js"></script>
        <script src="../../js/sweetalert2.all.min.js"></script>
        <script src="../../js/bootstrap.min.js"></script>
        <script src="../../js/script.js"></script>
        <link rel="icon" href="https://img.icons8.com/color/48/000000/globe--v1.png">
    </head>
    <body>
      
    <!-- menu de navigation -->
        <nav id="nav-menu" class="navbar navbar-expand-lg  bg-light px-lg-5">
            <a class="navbar-brand" href="http://'.$_SERVER['HTTP_HOST'].'">
                <img src="../../img/worlddb_logo.png" height="30" class="d-inline-block align-top" alt="">
            </a>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="http://'.$_SERVER['HTTP_HOST'].'/continent/all">Tout les pays<span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="http://'.$_SERVER['HTTP_HOST'].'/city/show/all">Toutes les villes<span class="sr-only">(current)</span></a>
                    </li>
                </ul>
                <form method="post" action="http://'.$_SERVER['HTTP_HOST'].'/search/" class="form-inline my-2 mx-2 my-lg-0">
                    <input name="search" class="form-control mr-sm-2" type="search" placeholder="Rechercher villes" aria-label="Search">
                        <button class="btn btn-outline-success my-2 my-sm-0" value="submit" type="submit">Recherche</button>
                </form>
            </div>
            ';

if (!isset($_SESSION['login'])) {
    echo '<form style="margin-block-end:0" method="post" action="http://'.$_SERVER['HTTP_HOST'].'/connexion">';
    echo '<input type="hidden" name="link" value="'.$_SERVER['REQUEST_URI'].'">';
    echo '<button type="submit" class="btn btn-primary">se connecter</button>';
    echo '</form>';
}else{
    echo '<form style="margin-block-end:0" method="post" action="http://'.$_SERVER['HTTP_HOST'].'/deconnexion">';
    echo '<input type="hidden" name="link" value="'.$_SERVER['REQUEST_URI'].'">';
    echo '<button type="submit" class="btn btn-danger active">se déconnecter</button>';
    echo '</form>';
}

echo '</nav>
        <div class="container">
            <div class="row mt-5">';

