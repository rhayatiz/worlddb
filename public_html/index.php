<?php
session_start();

require_once '../src/controller/CityController.php';
require_once '../src/controller/CountryController.php';
require_once '../src/controller/UserController.php';
require_once '../src/model/DAOCity.php';
require_once '../src/model/DAOCountry.php';
require_once '../src/model/DAOUser.php';
require_once '../src/utils/Renderer.php';

if (isset($_SERVER["PATH_INFO"])) {
    $path = trim($_SERVER["PATH_INFO"], "/");
} else {
    $path = "";
}

$fragments = explode("/", $path);
$control = array_shift($fragments);

switch ($control) {
    case '': { //l'url est /
            renderer::render('accueil');
            break;
        }
    case "city": {
            if ($_SERVER["REQUEST_METHOD"] == "GET") {
                cityRoutes_get($fragments);
            } else if ($_SERVER["REQUEST_METHOD"] == "POST") {
                cityRoutes_post($fragments);
            }
            break;
        }
    case "country": {
            if ($_SERVER["REQUEST_METHOD"] == "GET") {
                countryRoutes_get($fragments);
            } else if ($_SERVER["REQUEST_METHOD"] == "POST") {
                countryRoutes_post($fragments);
            }
            break;
        }
    case "continent": {
            if ($_SERVER["REQUEST_METHOD"] == "GET") {
                continentRoutes_get($fragments);
            }else if ($_SERVER["REQUEST_METHOD"] == "POST") {
                continentRoutes_post($fragments);
            }
            break;
        }
    case "connexion": {
            if (isset($_POST['login']) && (isset($_POST['password']))){
                $data = array($_POST['login'], $_POST['password']);
                call_user_func_array(['UserController', 'verify'], array($data));
            }else{
                renderer::render('connexion');
            }
            break;
        }
    case "deconnexion": {
            renderer::render('deconnexion');
            break;
    }
    default: {
        if (isset($_POST['search'])){
            header('Location: http://'.$_SERVER['HTTP_HOST']."/city/search/".$_POST['search']);
        }
        echo "erreur";
        break;
        }
}

function cityRoutes_get($fragments)
{
    $action = array_shift($fragments);

    switch ($action) {
        case "show":{
            call_user_func_array(["CityController", "display"], $fragments);
            break;
        }
        case "search":{
            call_user_func_array(["CityController", "search"], $fragments);
            break;
        }
        case "delete": {
                call_user_func_array(["CityController", "delete"], $fragments);
                break;
            }
        default: {
                echo "Action '$action' non definie <hr>";
            }
    }
}

function countryRoutes_get($fragments)
{
    $continents = array('Asia', 'Europe', 'North America', 'Africa', 'Oceania', 'Antarctica', 'South America');
    $action = array_shift($fragments);


    switch ($action) {
        case "show": {
                call_user_func_array(["CountryController", "show"], $fragments);
                break;
            }
        default: {
                if (in_array($action, $continents)) {
                    call_user_func_array(["CountryController", "display"], array($action));
                } else {
                    echo "Action " . array($action) . "non definie <hr>";
                }
            }
    }
}

function continentRoutes_get($fragments)
{
    $continents = array('Asia', 'Europe', 'North America', 'Africa', 'Oceania', 'Antarctica', 'South America','all');
    $c = $fragments;

    if (in_array(array_shift($fragments), $continents)) {
        call_user_func_array(["CountryController", "display"], $c);
    } else {
        echo "erreur continentRoutes_get";
    }
}

function cityRoutes_post($fragments)
{
    $action = array_shift($fragments);

    switch($action){
        case "delete":{
            call_user_func_array(["CityController", "delete"], $fragments);
            break;
        }
        case "update":{
            array_push($fragments,$_POST['Name'], $_POST['District'], $_POST['Population'], $_SERVER['HTTP_REFERER']);
            call_user_func_array(["CityController", "update"], array($fragments));
            break;
        }
        case "save":{
            array_push($fragments,$_POST['Name'], $_POST['District'], $_POST['Population'],$_POST['CountryCode'], $_SERVER['HTTP_REFERER']);
            call_user_func_array(["CityController", "save"], array($fragments));
            break;
        }

    }
}
