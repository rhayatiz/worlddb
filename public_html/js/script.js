function JSConfirm(){
    Swal.fire({
        title: 'Confirmer la suppression',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Supprimer!',
        cancelButtonText: 'Annuler'
      }).then((result) => {
        if (result.value) {
          this.remove();
        }
      })
};

function deleteVille(){
  Swal.fire('Ville supprimée!');
}

//Bootstrap modal (formulaire modification ville)
$('#myModal').on('shown.bs.modal', function () {
  $('#myInput').trigger('focus')
})

//Supression d'une ville
$(document).ready(function() {
  $(".delete-button").click(function() {
    //Supprimer la ville/pays de la base de données
    let id = $(this).parent().parent().attr("data-city-id");
    console.log(id);
    let xhr = new XMLHttpRequest();
    xhr.open('POST', '../../city/delete/'+id);
    xhr.send()
    // ../../country/delete/id

    //Supprimer la ligne de la ville/pays du tableau
    $(this).parent().parent().remove();
  });
});

// $(document).ready(function() {
//   $(".modify-button").click(function(e) {
//     //mettre des balises <input> au lieu de <a>
//     var ch = $(this).parent().parent().children();
//     for (var i = 0; i < ch.length - 1; i++)
//     {
//         console.log(ch[i].firstChild.innerHTML);
//         let text = ch[i].firstChild.innerHTML;
//     		ch[i].firstChild.replaceWith('<input class="focus" placeholder="'+text+'">');
//         // ch[i].firstChild.remove();
//         // let inp = document.createElement("input");
//         // ch[i].appendChild(e);
//     }
//   });
// });


